const {
    Order
} = require("./models");

const ordersData = require('./orders.json');

Order.bulkCreate(ordersData)
  .then(() => {
    console.log('Data seeded successfully');
  })
  .catch((error) => {
    console.error('Error seeding data:', error);
  });

// let data = {
//     poid: 123456789,
//     user_id: 'akmal',
//     service_id: 1,
//     target: '123456789',
//     quantity: 123456789,
//     start_count: 123456789,
//     remains: 123456789,
//     price: 123456789,
//     status: '123456789',
//     place_from: '123456789',
//     notes: '123456789',
//     refund: true,
//     created_at: new Date(),
//     updated_at: new Date()
// }

// Order.create(data)
//     .then((result) => {
//         console.log(result);
//     }).catch((err) => {
//         console.log("error" + err);
//     });