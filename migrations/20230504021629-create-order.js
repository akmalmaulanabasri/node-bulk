'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('orders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      poid: {
        type: Sequelize.BIGINT
      },
      user_id: {
        type: Sequelize.STRING
      },
      service_name: {
        type: Sequelize.STRING
      },
      target: {
        type: Sequelize.TEXT
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      start_count: {
        type: Sequelize.INTEGER
      },
      remains: {
        type: Sequelize.INTEGER
      },
      price: {
        type: Sequelize.INTEGER
      },
      profit: {
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.STRING
      },
      place_from: {
        type: Sequelize.STRING
      },
      notes: {
        type: Sequelize.STRING
      },
      refund: {
        type: Sequelize.BOOLEAN
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('orders');
  }
};