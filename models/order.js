'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Order.init({
    poid: DataTypes.BIGINT,
    user_id: DataTypes.STRING,
    service_name: DataTypes.TEXT,
    target: DataTypes.TEXT,
    quantity: DataTypes.INTEGER,
    start_count: DataTypes.INTEGER,
    remains: DataTypes.INTEGER,
    price: DataTypes.INTEGER,
    profit: DataTypes.INTEGER,
    status: DataTypes.STRING,
    place_from: DataTypes.STRING,
    notes: DataTypes.STRING,
    refund: DataTypes.BOOLEAN,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Order',
    timestamps: false
  });
  return Order;
};